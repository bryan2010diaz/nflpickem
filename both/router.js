"use strict";

Router.route('/', {
    name: "home",
    template: "home"
});

Router.route('/lobby', {
    name: "lobby",
    template: "lobby"
});

Router.route('/events', {
    name: "events",
    template: "events"
});

Router.route('/results', {
    name: "results",
    template: "results"
});

Router.route('/leaderboard', {
    name: "leaderboard",
    template: "leaderboard"
});