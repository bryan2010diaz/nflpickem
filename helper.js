"use strict";

if (Meteor.isClient) {
    console.log("Hello world");

    Template.leaderboard.helpers({
        'player': function () {
            var currentUserId = Meteor.userId();
            return PlayersList.find({
                createdBy: currentUserId
            }, {
                sort: {
                    score: -1,
                    name: 1
                }
            });
        },
        'selectedClass': function () {
            var playerId = this._id,
                selectedPlayer = Session.get('selectedPlayer');
            if (playerId === selectedPlayer) {
                return "selected";
            }
        },
        'showSelectedPlayer': function () {
            var selectedPlayer = Session.get('selectedPlayer');
            return PlayersList.findOne(selectedPlayer);
        }
    });

    Template.endbox.helpers({
        currentRoute: function (routeName) {
            return Router.current().route.getName() === routeName;
        }
    });
//    Template.gamelist.helpers({
//        currentRoute: function (routeName) {
//            return Router.current().route.getName() === routeName;
//        }
//    });
}