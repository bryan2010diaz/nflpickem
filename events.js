"use strict";

if (Meteor.isClient) {
    console.log("Hello client");

    Template.leaderboard.events({
        'click .player': function () {
            var playerId = this._id;
            Session.set('selectedPlayer', playerId);
        },
        'click .increment': function () {
            var selectedPlayer = Session.get('selectedPlayer');
            PlayersList.update(selectedPlayer, {
                $inc: {
                    score: 5
                }
            });
        },
        'click .decrement': function () {
            var selectedPlayer = Session.get('selectedPlayer');
            PlayersList.update(selectedPlayer, {
                $inc: {
                    score: -5
                }
            });
        },
        'click .remove': function () {
            var selectedPlayer = Session.get('selectedPlayer');
            PlayersList.remove(selectedPlayer);
        }
    });

    Template.lobby.events({
        'submit form': function (event) {
            event.preventDefault();
            var hometeam = $('.home-team').val(),
                awayteam = $('.away-team').val(),
                playdate = $('.playdate').val();
            PlayOffs.insert({
                hometeam: hometeam,
                awayteam: awayteam,
                created: new Date(),
                inserted: playdate
            });

        }
    });
}